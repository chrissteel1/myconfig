(add-hook 'org-mode-hook
         (lambda () (add-hook 'after-save-hook #'org-babel-tangle
                          :append :local)))

;; open debugger on error
(setq debug-on-error t)

;; Install straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(require 'package) ;; load a feature called package - which will load functions and variables from package
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/")t) ;; this adds melpa to package archives
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/") t)
;; (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize) ;; initializes the packaging system

;; If there are no archived package contents, refresh them
(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t))

(use-package company
  :ensure t
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  :config
  (setq company-dabbrev-downcase 0)
  (setq company-idle-delay 0.1)
  (setq company-minimum-prefix-length 1)
  (setq company-tooltip-align-annotations t))

(global-company-mode t)

(use-package magit
  :ensure t)

(use-package forge
:ensure t
:after magit)

(use-package git-timemachine
  :ensure t
  :defer t
  :bind
  ("C-c h" . git-timemachine))

(use-package which-key
  :ensure t
  :defer t 
  :init (which-key-mode))

(use-package smartparens
  :ensure t
  :init
  (smartparens-global-mode))

(use-package doom-themes
  :ensure t
  :preface (defvar region-fg nil) ; this prevents a weird bug with doom themes
  :init (load-theme 'doom-solarized-light t))

(use-package treemacs
  :ensure t)

(add-hook 'emacs-startup-hook 'treemacs)
(add-hook 'emacs-startup-hook 'treemacs-toggle-show-dotfiles)

(add-to-list 'password-word-equivalents "totp verification code")

(setq tramp-default-method "ssh")

(use-package sudo-edit
  :ensure t)

(use-package ess
    :ensure t)

  (use-package poly-R
  :ensure t)


  ;; mode for working with rmd files
  (require 'poly-R)

(use-package poly-R
  :ensure t)



;; mode for working with rmd files
(require 'poly-R)

  ;; associate the new polymode to Rmd files:
  (add-to-list 'auto-mode-alist
	       '("\\.[rR]md\\'" . poly-gfm+r-mode))

  ;; uses braces around code block language strings:
  (setq markdown-code-block-braces t)

(use-package elpy
  :ensure t
  :init
  (elpy-enable))

;; Use IPython for REPL
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters
             "jupyter")

(use-package ein)

(use-package flycheck
  :ensure t)

;; Enable Flycheck
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

(use-package vterm
  :ensure t)

(use-package ox-pandoc
  :ensure t)

;;  :custom
	;;  (org-cite-global-bibliography
	 ;;  '("~/Nextcloud/Bib/Library.bib"))


	 ;; (use-package citar
	 ;;   :straight (citar :type git :host github :repo "emacs-citar/citar" :includes (citar-org))
	 ;;   :config
	 ;;   (setq citar-bibliography '("~/Nextcloud/Bib/Library.bib")))


(use-package citar
  :no-require
  :custom
  (org-cite-global-bibliography '("~/Nextcloud/Bib/Library.bib"))
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  (citar-bibliography org-cite-global-bibliography)
  ;; optional: org-cite-insert is also bound to C-c C-x C-@
  :bind
  (:map org-mode-map :package org ("C-c b" . #'org-cite-insert)))



      ;; (use-package citar
      ;; :no-require
      ;; :custom
      ;; (org-cite-global-bibliography '("~/Nextcloud/Bib/Library.bib"))
      ;; (org-cite-insert-processor 'citar)
      ;; (org-cite-follow-processor 'citar)
      ;; (org-cite-activate-processor 'citar)
      ;; (citar-bibliography org-cite-global-bibliography)
      ;; ;; optional: org-cite-insert is also bound to C-c C-x C-@
      ;; :bind
      ;; (:map org-mode-map :package org ("C-c b" . #'org-cite-insert)))

(use-package vertico
  :straight t
  :init
  (setq
   vertico-scroll-margin 0
   vertico-count 20
   vertico-resize t
   vertico-cycle t)
  (vertico-mode))


(use-package orderless
  :straight t
  :custom
  (completion-styles '(orderless flex basic))
  (completion-category-overrides '((file (styles . (partial-completion))))))

(use-package consult
  :straight t
  :config
  (setq consult-fontify-max-size 1024))

(use-package embark
  :straight t
  :bind
  (("C-." . embark-act)
   ("C-h B" . embark-bindings))
  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
		 '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
		   nil
		   (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :straight t
  :after (embark consult)
  :demand t ; only necessary if you have the hook below
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package marginalia
  :straight t
  :defer t
  :init
  (marginalia-mode)
  (setq marginalia-annotators '(marginalia-annotators-heavy nil)))

(use-package yasnippet
       :ensure t
       :init
       (yas-global-mode 1)
       :config
       (add-to-list 'yas-snippet-dirs (locate-user-emacs-file "~/myconfig/snippets")))

(add-hook 'emacs-startup-hook (lambda () (yas-load-directory "~/myconfig/snippets")))

(use-package simple-httpd
  :ensure t)

;;; remove validation link when website publishing
(setq org-html-validation-link nil)

(use-package rainbow-delimiters 
  :ensure t)

(setq rainbow-delimiters-mode t) ;; activate on startup

;; (use-package undo-fu-session 
;;  :ensure t)

;; (global-undo-fu-session-mode) ;; activate on startup

(use-package org-bullets 
  :ensure t)

(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(use-package golden-ratio
   :ensure t)

(require 'golden-ratio)

(golden-ratio-mode 1)

;; Make it very easy to see the line with the cursor.
(global-hl-line-mode t)

(use-package beacon
  :diminish
  :config (setq beacon-color "#666600")
  :hook   ((org-mode text-mode) . beacon-mode))

(use-package snakemake-mode
  :ensure t)

(require 'snakemake-mode)

(use-package conda
  :ensure t)

(require 'conda)

(use-package synosaurus
  :ensure t)

(require 'synosaurus)

(use-package org-wc
  :ensure t)

(require 'org-wc)

(use-package dashboard
    :ensure t
    :config
    (dashboard-setup-startup-hook))


(require 'dashboard)
(dashboard-setup-startup-hook)
;; Or if you use use-package

(use-package org-inline-pdf
  :ensure t)

(require 'org-inline-pdf)

(add-hook 'org-mode-hook #'org-inline-pdf-mode)

;; show time on mode line

(setq display-time-day-and-date t
display-time-24hr-format t)
(display-time)



  (custom-set-variables '(confirm-kill-processes nil))


  (tool-bar-mode 0) 

  (menu-bar-mode 0)

  (scroll-bar-mode 0)

  (setq ring-bell-function 'ignore)

  (setq inhibit-startup-screen 1)

  ;; gives you the option to save window/buffer configuration on exit
 ;;desktop-save-mode 1)
  
  (setq initial-buffer-choice "~/Nextcloud/Master_TODO.org")

(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

(global-display-line-numbers-mode 1) ;; Enable line numbers globally

(fset 'yes-or-no-p 'y-or-n-p)

(setq user-init-file "~/.emacs.d/init.el")

;; (require 'magit)
;; (defun commit-and-push-config ()
;;   (interactive) ;; Make this an interactivley callable command
;;   (let* ((default-directory (expand-file-name "myconfig" "~"))
;;          (config-file (expand-file-name "init.org" default-directory)))
;;     (when (magit-anything-modified-p nil config-file)
;;       (magit-call-git "add" config-file)
;;       (magit-call-git "commit" "-m" "init.org auto-save update")
;;       (magit-call-git "push" "origin")
;;       (magit-refresh))))

;; (add-hook 'kill-emacs-hook #'commit-and-push-config)

;;   (require 'ox-latex)
;; (unless (boundp 'org-latex-classes)
;;   (setq org-latex-classes nil))
;; (add-to-list 'org-latex-classes
;;              '("article"
;;                "\\documentclass{article}"
;;                ("\\section{%s}" . "\\section*{%s}")))


;;   (add-to-list 'org-latex-classes
;; 	       '("tikzposter" "\\documentclass{tikzposter}"
;; 		 ("\\section{%s}" . "\\section*{%s}")
;; 		 ("\\subsection{%s}" . "\\subsection*{%s}")))

(use-package org
      :ensure t)

  (require 'org)

  (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

   ;; got rid of this, for now atleast.. I think I end up spending more time collapsing and this way will be more efficient
   ;; collapse org docs on opening
  (setq org-startup-folded t)   

(org-babel-do-load-languages
   'org-babel-load-languages
   '((R . t)
     (emacs-lisp . t)
     (shell . t)
     (dot . t)
     (latex . t)
     (python . t)
     (js . t)
     (awk . t)))

  ;; stop images from being giant rendered in org 
  (setq org-image-actual-width nil)

;; (use-package org-contrib
;;     :config
;;     (require 'ox-extra)
;;     (ox-extras-activate '(ignore-headlines)))

(global-set-key (kbd "C-c v") 'vterm)

(global-set-key (kbd "C-c q") "ssh cjs236@login-cpu.hpc.cam.ac.uk")

(global-set-key (kbd "C-c b") "#+BEGIN_SRC \n\n#+END_SRC")


(global-set-key (kbd "C-c s") "#+BEGIN_SRC R :session :results none \n\n#+END_SRC")


(global-set-key (kbd "C-c p") "#+TITLE: \n#+AUTHOR: Chris Steel \n#+DATE: \n#+SETUPFILE: ./html_themes/solarized_light.theme \n#+EXPORT_FILE_NAME: exports/ \n#+cite_export: csl ~/Nextcloud/org_workflows/cite_styles/Nature.csl")


(defun test ()
 interactive)

(define-key org-mode-map (kbd "C-c t") 'toggle-truncate-lines)

(defun hrs/visit-emacs-config ()
  (interactive)
  (find-file "~/myconfig/init.org"))

(global-set-key (kbd "C-c c") 'hrs/visit-emacs-config)

(find-file "~/Nextcloud/Master_TODO.org") 


;; Start maximised (cross-platf)
(add-hook 'window-setup-hook 'toggle-frame-maximized t)

(setq org-latex-packages-alist '(("margin=2cm" "geometry" nil)))
