;;; Compiled snippets and support files for `org-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'org-mode
		     '(("yastime"
			(error
			 (error-message-string
			  (invalid-read-syntax ")")))
			"time_current" nil nil nil "/home/chris/myconfig/snippets/org-mode/time_stamp" nil nil)
		       ("pybabel" "#+BEGIN_SRC python :results output\n\n#+END_SRC\n" "pybabel" nil nil nil "/home/chris/myconfig/snippets/org-mode/py_babel" nil nil)
		       ("<org" "testing testing" "org" nil nil nil "/home/chris/myconfig/snippets/org-mode/org" nil nil)
		       ("intersect" "∩" "intersect" nil nil nil "/home/chris/myconfig/snippets/org-mode/intersection" nil nil)
		       ("ggsave" "ggsave(\"\", plot= last_plot(), width = 4, height = 4, dpi=300)" "ggpsave" nil nil nil "/home/chris/myconfig/snippets/org-mode/ggsave" nil nil)
		       ("ggtemplate" "<- ggplot( ,aes()) + geom_point() + geom_smooth(se=F, color='black') + labs(title='') + scale_x_log10() +\ntheme(panel.background = element_rect(fill = \"gray98\", colour = \"white\", linewidth = 2, linetype = \"solid\"),\n        panel.grid.major = element_line(linewidth = 0.5, linetype = 'dotted', colour = \"black\"), \n        panel.grid.minor = element_line(linewidth = 0.25, linetype = 'solid', colour = \"black\"), \n        panel.border = element_rect(colour = \"black\", fill=NA, linewidth=1),\n        axis.text.x = element_text(size = 10),\n        axis.text.y = element_text(size = 10),\n        axis.title = element_text(size = 10),\n        legend.text = element_text(size = 10), plot.title = element_text(size=20), legend.title = element_blank())" "ggplotbase" nil nil nil "/home/chris/myconfig/snippets/org-mode/ggplot" nil nil)))


;;; Do not edit! File generated at Wed Jan 11 14:57:55 2023
