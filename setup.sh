
pkg_installer="sudo apt install -y"

pkgs="emacs git curl libssl-dev libcurl4-openssl-dev libxml2-dev cmake libgnutls28-dev \
libasound2-dev libtool-bin libvterm-dev autoconf libgtk-3-dev \
texinfo libgccjit-11-dev libjansson-dev librsvg2-dev libsqlite3-dev librsvg2-bin jupyter pandoc texlive-full"

R_packages='c("tidyverse")'

Python_libs="python3-jedi black python3-autopep8 yapf3 python3-yapf"

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# setup
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

# This echos whatever we pass into it in a highlighted font

function message {
      echo $(tput smso)$(tput setaf 163)"$*"$(tput sgr0)
}

# update apt packages

function update_apt {
    apt-get update
}

# install dependency packages and python libraries

function setup_packages {
    $pkg_installer $pkgs $Python_libs
}

# install R and wanted R packages

function setup_R {
    $pkg_installer r-recommended R -e "install.packages(setdiff($R_packages, rownames(installed.packages())))"
}


# get config repo 

function get_config_files {
    if [~/myconfig];
    then
	echo "Your myconfig directory is already here!"
    else
        git clone https://gitlab.com/chrissteel1/myconfig.git && ln -s ~/myconfig/init.el .emacs.d/init.el && rm .bashrc && ln -s ~/myconfig/.bashrc .bashrc
    fi
}
		
# Function which calls on all previously defined functions

function setup_all {
    setup_packages
    setup_R
    get_config_files
}

# Firstly update apt, then run the rest of script

update_apt && message apt has been updated && setup_all && message set up has finished running










